import './App.css';
import React, {useState, useEffect} from "react";

function App() {
    const [data, setData] = useState([]);
    const [currentPage, setCurrentPage] = useState(1);
    const [perPage] = useState(10);

    const skip = (currentPage - 1) * perPage;

    useEffect(() => {
        fetch(`http://localhost:3000/products?limit=${perPage}&skip=${skip}`)
            .then((response) => response.json())
            .then((data) => setData(data))
            .catch((error) => console.error(error));
    }, [currentPage, perPage, skip]);

    const [expanded, setExpanded] = useState({});
    const toggleExpanded = (item) => {
        setExpanded(prevState => ({
            ...prevState,
            [item.name]: !prevState[item.name]
        }));
    }

    const renderDetails = (item) => {
        if (expanded[item.name]) {
            return (
                <div>
                    {item.details}
                    <div className="div__center">
                        <button onClick={() => toggleExpanded(item)} className="button">Show less</button>
                    </div>
                </div>
            );
        } else {
            return (
                <div>
                    {item.details.split(" ").slice(0, 6).join(" ")}{'...'}
                    <div className="div__center">
                        <button onClick={() => toggleExpanded(item)} className="button">See details</button>
                    </div>
                </div>
            );
        }
    };

    const renderImage = (data) => {
        return <img alt="image" className="image" src={data}/>
    }

    const handlePrevPage = () => {
        setCurrentPage(currentPage => Math.max(currentPage - 1, 1));
    }

    const handleNextPage = () => {
        setCurrentPage(currentPage => currentPage + 1);
    }

    return (
        <>
            <table>
                <thead className="header__center">
                <h1>See Products</h1>
                </thead>
                <tbody className="table__body">
                {data.map((item) => (
                    <tr className="table__row" key={item.name}>
                        <td>
                            <div className="image__container">{renderImage(item.image)}
                                <span className="discount">{item.discount}</span>
                            </div>
                        </td>
                        <td className="align__name__price"><span>{item.name}</span>
                            <span>{item.price}</span></td>
                        <td className="div__center">{renderDetails(item)}</td>
                    </tr>
                ))}
                </tbody>
            </table>
            <div className="pagination">
                <button className="pagination--button" onClick={handlePrevPage} disabled={currentPage === 1}>Prev</button>
                <span className="pagination__span">{currentPage}</span>
                <button className="pagination--button" onClick={handleNextPage} disabled={data.length < perPage}>Next</button>
            </div>
        </>
    );
}

export default App;
